# do not produce empty debuginfo package
%global debug_package %{nil}

Summary: Dummy package for testing and onboarding purposes
Name: redhat-internal-test-package
Version: 0.1
Release: 4%{?dist}
License: GPLv2+
Group: Applications/File
Source0: README
Source1: LICENSE

%description
Dummy package that is meant to be used for testing or onboarding purposes,
but which should not ever get to a real release.

%prep
%setup -c -T

# This section generates README file from a template and creates man page
# from that file, expanding RPM macros in the template file.
cat <<'EOF' | tee README
%{expand:%(cat %{SOURCE0})}
EOF

# copy the license file so %%files section sees it
cp %{SOURCE1} .

%build

%install
mkdir -p %{buildroot}%{_bindir}
cat <<'EOF' | tee %{buildroot}%{_bindir}/%{name}
#!/usr/bin/bash
echo "Oh, it's doing something!"
EOF
chmod a+x %{buildroot}%{_bindir}/%{name}

%files
%doc README LICENSE
%{_bindir}/%{name}

%changelog
* Mon May 02 2022 Honza Horak <hhorak@redhat.com> - 0.1-4
- Add dummy entry

* Thu Mar 24 2022 Honza Horak <hhorak@redhat.com> - 0.1-3
- Onboarding hhorak

* Wed Mar 02 2022 Honza Horak <hhorak@redhat.com> - 0.1-2
- Release bump

* Tue Mar 01 2022 Honza Horak <hhorak@redhat.com> - 0.1-1
- Initial packaging
